#ifndef __CF4RICH_H__
#define __CF4RICH_H__
	
//
//  Hello CF4rich Fans:
//
//  This code implements analytical parameterizations of the measured 
//  performance from the eRD6 RICH prototype.
//
//  There is additionally a macro that makes comparison
//  between the measured data and the calculations from this routine.
//
//  This routine assumes units of cm for distances and picoSeconds for time.
//
//                                 TKH PG March 2020
//

#include "PID.h"
	
class CF4rich: public PID
{
public:
  CF4rich(double length=100, double etaLow=1.0, double etaHigh=4.0, double padsize=0.5, double dp_over_p=0.05, double CONST = 245);
  virtual ~CF4rich() {}
	
  bool   valid   (double eta, double p);
  double numSigma(double eta, double p,        PID::type PID);
  double maxP    (double eta, double numSigma, PID::type PID);
  double minP    (double eta, double numSigma, PID::type PID);
  string name    () {return myName;}
  void   description ();
		
protected:
  std::string myName;

  // utility function
  double radius(double p, double m);
  double width(double p, double m);

  // Detector Parameters:
  double length;   // cm
  double etaLow;
  double etaHigh;
  double padsize;   // cm

  //  Spectrometer Performance:
  double dp_over_p;  //  dp_over_p...unitless

  // Physical constants (should come from elsewhere!)
  double mPion;    // GeV/c^2
  double mKaon;    // GeV/c^2
  double mProton;  // GeV/c^2
  double c;        // cm/picosecond;
  double n;        // index of CF4...unitless
  double dispersion;  //  fractional radius variation due to dispersion...unitless...
  double constant;  //  Additional constant term from test beam data...microns...

};
	
#endif /* __PID_H__ */
